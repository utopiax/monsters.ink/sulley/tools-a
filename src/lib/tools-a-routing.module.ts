import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Test1Component } from './test1/test1.component';

@NgModule({
  imports: [
    RouterModule.forChild([{ path: 'test', component: Test1Component }, { path: '', redirectTo: 'test', pathMatch: 'full' }])
  ],
  exports: [RouterModule]
})
export class ToolsARoutingModule {
}
