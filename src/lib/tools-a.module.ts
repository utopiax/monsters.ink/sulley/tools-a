import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Test1Component } from './test1/test1.component';
import { ToolsARoutingModule } from './tools-a-routing.module';

@NgModule({
  imports: [CommonModule, ToolsARoutingModule],
  declarations: [Test1Component],
})
export class ToolsAModule {}
